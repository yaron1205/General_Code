# -*- coding: utf-8 -*-
import numpy as np
import rasterio   
import os 
from osgeo import gdal_array
import xarray as xr


rasterfilename = []
rasters =[]
#ws = 'D:/TEST-GEO-TIF/MCD43A4test'

ws = 'D:/SatelliteData/ALPOO'

for folder, subs, files in os.walk(ws):
    for filename in files:
        print filename
        rasterfilename.append(filename)
        aSrcF = gdal_array.LoadFile(os.path.join(folder,filename))
        #aSrc = np.flipud(aSrcF)#FILEP THE RASTER
        #aSrc[aSrc== -1.79769300e+308] = np.nan
        rasters.append(aSrcF)
stackRast = np.dstack(rasters)
src = rasterio.open(r'D:\SatelliteData\ALPOO\2013.tif')
stackRast.shape
kwargs = src.meta
num_of_reaster =len(rasters)

#some file
xr.DataArray(stackRast)

da = xr.DataArray(stackRast, dims=('lat','lon', 'time'))
#da = xr.DataArray(stackRast, dims=('x', 'y', 'time'))


def linear_trend(x):
    pf = np.polyfit(x.time, x, 1)

    return xr.DataArray(pf[0])

# stack lat and lon into a single dimension called allpoints
stacked = da.stack(allpoints=['lat','lon'])
# apply the function over allpoints to calculate the trend at each point
trend = stacked.groupby('allpoints').apply(linear_trend)
trend_unstacked = trend.unstack('allpoints')
print trend_unstacked
trend_unstacked.plot()

with rasterio.open(r'D:\SatelliteData\ALPOO\PR.tif', 'w', **kwargs) as dst:
         dst.write_band(1, trend_unstacked.astype(rasterio.float32))

